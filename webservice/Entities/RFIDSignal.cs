﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class RFIDSignal
    {
      public String RFIDTag{get;set;}
      public DateTime? UpdatedDate {get;set;}
      public String? AntennaID{get;set;}
      public String? TagSeenCount{get;set;}
      public String? PeakRSSI{get;set;}
      public String? PC{get;set;}
      public String? MemoryBankData{get;set;}
      public String? Hostname{get;set;}
      public String? Port{get;set;}
      public DateTime? InsertedDate {get;set;}
      public String? AssetID{get;set;}
      public String? AssetName{get;set;}
    }
}
