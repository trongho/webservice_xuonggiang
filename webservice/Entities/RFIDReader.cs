﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace webservice.Entities
{
    public class RFIDReader
    {
      public String HostName{get;set;}
      public String? Port{get;set;}
      public String? Model{get;set;}
      public String? ReaderID{get;set;}
      public String? Firmware{get;set;}
      public String? SerialNumber{get;set;}
      public DateTime? MfgDate{get;set;}
      public String? Description{get;set;}
      public String? DepartmentID{get;set;}
      public String? Status{get;set;}
      public String? CreatedUserID{get;set;}
      public DateTime? CreatedDate {get;set;}
      public String? UpdatedUserID{get;set;}
      public DateTime? UpdatedDate {get;set;}
    }
}
