﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WIRDetailHelper
    {
        public static List<WIRDetailModel> Covert(List<WIRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WIRDetailModel
            {
                WIRNumber = sc.WIRNumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsName = sc.GoodsName,
                OtherGoodsName = sc.OtherGoodsName,
                PackingVolume = sc.PackingVolume,
                TotalQuantity = sc.TotalQuantity,
                QuantityByItem = sc.QuantityByItem,
                QuantityByPack = sc.QuantityByPack,
                LocationID = sc.LocationID,
                ScanOption = sc.ScanOption,
                Note = sc.Note,
                Status = sc.Status,
                SupplierCode = sc.SupplierCode,
                ASNNumber = sc.ASNNumber,
                PackingSlip = sc.PackingSlip,
                QuantityShipped = sc.QuantityShipped,
            });

            return models;
        }
    }
}
