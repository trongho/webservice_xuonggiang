﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class RFIDSignalHelper
    {
        public static List<RFIDSignalModel> Covert(List<RFIDSignal> entrys)
        {
            var models = entrys.ConvertAll(sc => new RFIDSignalModel
            {
                RFIDTag=sc.RFIDTag,
                UpdatedDate=sc.UpdatedDate,
                AntennaID=sc.AntennaID,
                TagSeenCount=sc.TagSeenCount,
                PeakRSSI=sc.PeakRSSI,
                PC=sc.PC,
                MemoryBankData=sc.MemoryBankData,
                Hostname=sc.Hostname,
                Port=sc.Port,
                InsertedDate=sc.InsertedDate,
                AssetID=sc.AssetID,
                AssetName=sc.AssetName,
                
            });

            return models;
        }
    }
}
