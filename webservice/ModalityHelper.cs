﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class ModalityHelper
    {
        public static List<ModalityModel> Covert(List<Modality> entrys)
        {
            var models = entrys.ConvertAll(sc => new ModalityModel
            {
                ModalityID = sc.ModalityID,
                Ordinal = sc.Ordinal,
                ModalityName = sc.ModalityName,
                ModalityNameEN=sc.ModalityNameEN,
                Type=sc.Type,
                Description=sc.Description,
                Status=sc.Status,

            });

            return models;
        }
    }
}
