﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UnitHelper
    {
        public static List<UnitModel> Covert(List<Unit> entrys)
        {
            var models = entrys.ConvertAll(sc => new UnitModel
            {
                UnitID=sc.UnitID,
                UnitRate=sc.UnitRate,
                StockUnitID=sc.StockUnitID,
                Type=sc.Type,
                Description=sc.Description,
                Status=sc.Status,
                CreatedUserID=sc.CreatedUserID,
                CreatedDate=sc.CreatedDate,
                UpdatedUserID=sc.UpdatedUserID,
                UpdatedDate=sc.UpdatedDate,
            });

            return models;
        }
    }
}
