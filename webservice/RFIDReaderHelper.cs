﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class RFIDReaderHelper
    {
        public static List<RFIDReaderModel> Covert(List<RFIDReader> entrys)
        {
            var models = entrys.ConvertAll(sc => new RFIDReaderModel
            {
                HostName = sc.HostName,
                Port = sc.Port,
                Model=sc.Model,
                ReaderID=sc.ReaderID,
                Firmware=sc.Firmware,
                SerialNumber=sc.SerialNumber,
                MfgDate=sc.MfgDate,
                Description=sc.Description,
                DepartmentID=sc.DepartmentID,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
