﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class IOHeaderHelper
    {
        public static List<IOHeaderModel> Covert(List<IOHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new IOHeaderModel
            {
                IONumber= sc.IONumber,
                ModalityID = sc.ModalityID,
                ModalityName = sc.ModalityName,
                IODate = sc.IODate,
                ReferenceNumber = sc.ReferenceNumber,
                WIRNumber = sc.WIRNumber,
                WIRReference = sc.WIRReference,
                WarehouseID=sc.WarehouseID,
                WarehouseName=sc.WarehouseName,
                ToWarehouseID = sc.ToWarehouseID,
                ToWarehouseName = sc.ToWarehouseName,
                SupplierID = sc.SupplierID,
                SupplierName = sc.SupplierName,
                CustomerID = sc.CustomerID,
                CustomerName = sc.CustomerName,
                OrderNumber = sc.OrderNumber,
                OrderDate = sc.OrderDate,
                ContractNumber = sc.ContractNumber,
                ContractDate = sc.ContractDate,
                RONumber = sc.RONumber,
                PriceTypeID = sc.PriceTypeID,
                PriceTypeName = sc.PriceTypeName,
                PaymentTypeID = sc.PaymentTypeID,
                PaymentTypeName = sc.PaymentTypeName,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                LocalOrderType = sc.LocalOrderType,
                TotalDiscountAmount = sc.TotalDiscountAmount,
                TotalVATAmount = sc.TotalVATAmount,
                TotalAmountExcludedVAT = sc.TotalAmountExcludedVAT,
                TotalAmountIncludedVAT = sc.TotalAmountIncludedVAT,
                TotalAmount = sc.TotalAmount,
                TotalQuantity = sc.TotalQuantity,
                Note = sc.Note,
                CheckerID1 = sc.CheckerID1,
                CheckerName1 = sc.CheckerName1,
                CheckerID2 = sc.CheckerID2,
                CheckerName2 = sc.CheckerName2,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
