﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRRDetailHelper
    {
        public static List<WRRDetailModel> Covert(List<WRRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRRDetailModel
            {
                  WRRNumber=sc.WRRNumber,
                  Ordinal=sc.Ordinal,
                  RFIDTagID=sc.RFIDTagID,
                  Barcode=sc.Barcode,
                  StyleColorSize=sc.StyleColorSize,
                  Style=sc.Style,
                  Color=sc.Color,
                  Sku=sc.Sku,
                  Size=sc.Size,
                  Quantity=sc.Quantity,
                  Gender=sc.Gender,
                  ProductGroup=sc.ProductGroup,
                  DescriptionVN=sc.DescriptionVN,
                  LifeStylePerformance=sc.LifeStylePerformance,
                  Div=sc.Div,
                  Outsole=sc.Outsole,
                  Category=sc.Category,
                  Productline=sc.Productline,
                  Description=sc.Description,
                  EXWorkUSD=sc.EXWorkUSD,
                  EXWorkVND=sc.EXWorkVND,
                  AmountVND=sc.AmountVND,
                  RetailPrice=sc.RetailPrice,
                  Season=sc.Season,
                  Coo=sc.Coo,
                  Material=sc.Material,
                  InvoiceNo=sc.InvoiceNo,
                  DateOfInvoice=sc.DateOfInvoice,
                  SaleContract=sc.SaleContract,
                  Shipment=sc.Shipment,
                  FactoryAdress=sc.FactoryAdress,
                  FactoryName=sc.FactoryName,
                  FactoryNameVN=sc.FactoryNameVN,
                  FactoryAdressVN=sc.FactoryAdressVN,
                  NewPrice=sc.NewPrice,
                  ChangePrice=sc.ChangePrice,
                  Status=sc.Status
            });

            return models;
        }
    }
}
