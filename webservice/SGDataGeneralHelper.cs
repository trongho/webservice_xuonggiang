﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class SGDataGeneralHelper
    {
        public static List<SGDataGeneralModel> Covert(List<SGDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new SGDataGeneralModel
            {
                SGDNumber = sc.SGDNumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsName = sc.GoodsName,
                IDCode = sc.IDCode,
                LocationID = sc.LocationID,
                Quantity = sc.Quantity,
                TotalQuantity = sc.TotalQuantity,
                TotalGoods = sc.TotalGoods,
                QuantityOrg = sc.QuantityOrg,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                TotalGoodsOrg = sc.TotalGoodsOrg,
                LocationIDOrg = sc.LocationIDOrg,
                CreatorID = sc.CreatorID,
                CreatedDateTime = sc.CreatedDateTime,
                EditerID = sc.EditerID,
                EditedDateTime = sc.EditedDateTime,
                Status = sc.Status,
                PackingVolume = sc.PackingVolume,
                QuantityByPack = sc.QuantityByPack,
                QuantityByItem = sc.QuantityByItem,
                Note = sc.Note,
                ScanOption = sc.ScanOption,
                PackingQuantity = sc.PackingQuantity,
                GoodsGroupID = sc.GoodsGroupID,
                LotID = sc.LotID,
                PalletID = sc.PalletID,
                MFGDate = sc.MFGDate,
                EXPDate = sc.EXPDate,
                Remark = sc.Remark,
                InputDate = sc.InputDate,
                RFIDTagID = sc.RFIDTagID,
            });

            return models;
        }
    }
}
