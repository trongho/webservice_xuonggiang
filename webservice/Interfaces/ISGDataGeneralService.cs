﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ISGDataGeneralService
    {
        Task<List<SGDataGeneral>> GetAll();
        Task<List<SGDataGeneral>> GetUnderId(String wRDNumber);
        Task<List<SGDataGeneral>> GetUnderId(String wRDNumber, String RFIDTagID, int Ordinal);
        Task<List<SGDataGeneral>> GetUnderId(String wRDNumber, String RFIDTagID);
        Task<List<SGDataGeneral>> GetUnderIdCode(String wRDNumber, String RFIDTagID, String IDCode);
        Task<Boolean> Create(SGDataGeneral SGDataGeneral);
        Task<Boolean> Update(String wRDNumber, String RFIDTagID, int Ordinal, SGDataGeneral SGDataGeneral);
        Task<Boolean> Delete(String wRDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wRDNumber, String RFIDTagID);
    }
}
