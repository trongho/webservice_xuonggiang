﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IRFIDReaderService
    {
        Task<List<RFIDReader>> GetAll();
        Task<Boolean> Create(RFIDReader entry);
        Task<List<RFIDReader>> GetUnderId(String id);
        Task<Boolean> Update(String id, RFIDReader entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
