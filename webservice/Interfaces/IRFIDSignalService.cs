﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IRFIDSignalService
    {
        Task<List<RFIDSignal>> GetAll();
        Task<Boolean> Create(RFIDSignal entry);
        Task<List<RFIDSignal>> GetUnderId(String id);
        Task<Boolean> Update(String id, RFIDSignal entry);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
