﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ISGDataHeaderService
    {
        Task<List<SGDataHeader>> GetAll();
        Task<List<SGDataHeader>> GetUnderId(String id);
        Task<Boolean> Create(SGDataHeader SGDataHeader);
        Task<Boolean> Update(String id, SGDataHeader SGDataHeader);
        Task<Boolean> Delete(String id);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String id);

        Task<List<SGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate);
        Task<List<SGDataHeader>> GetUnderBranchID(String branchID);
        Task<List<SGDataHeader>> GetUnderHandlingStatusID(String handlingStatusID);

        Task<List<SGDataHeader>> GetUnderOrderStatusID(String orderStatusID);
    }
}
