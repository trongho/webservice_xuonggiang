﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWRDataGeneralService
    {
        Task<List<WRDataGeneral>> GetAll();
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String Barcode, int Ordinal);
        Task<List<WRDataGeneral>> GetUnderId(String wRDNumber, String Barcode);
        Task<List<WRDataGeneral>> CheckIdCode(String wRDNumber, String Barcode, String IDCode);
        Task<Boolean> Create(WRDataGeneral wRDataGeneral);
        Task<Boolean> Update(String wRDNumber, String Barcode, int Ordinal, WRDataGeneral wRDataGeneral);
        Task<Boolean> Delete(String wRDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wRDNumber,String Barcode);
    }
}
