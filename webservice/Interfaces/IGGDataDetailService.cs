﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IGGDataDetailService
    {
        Task<List<GGDataDetail>> GetAll();
        Task<List<GGDataDetail>> GetUnderId(String GGDNumber);
        Task<List<GGDataDetail>> GetUnderId(String GGDNumber, String goodsID, int Ordinal);
        Task<List<GGDataDetail>> GetUnderIdCode(String GGDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(GGDataDetail GGDataDetail);
        Task<Boolean> Update(String GGDNumber, String goodsID, int Ordinal, GGDataDetail GGDataDetail);
        Task<Boolean> Delete(String GGDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String GGDNumber, String goodIDs);
    }
}
