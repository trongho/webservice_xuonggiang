﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Requests;
using webservice.Responses;

namespace webservice.Interfaces
{
    public interface IUserService
    {
        Task<List<User>> GetAllUser();
        Task<List<User>> GetUserUnderId(String id);
        Task<List<User>> GetUserUnderUsername(String username);
        Task<Boolean> CreateUser(User user);
        Task<Boolean> UpdateUser(String id, User user);
        Task<Boolean> DeleteUser(String id);
        Task<LoginResponse> Login(LoginRequest loginRequest);
        Task<String> GetLastUserId();
        Task<Boolean> checkExist(String id);
    }
}
