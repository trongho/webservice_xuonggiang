﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface ITallyDataService
    {
        Task<Boolean> Create(TallyData entry);
        Task<List<TallyData>> GetUnderId(String id);
        Task<List<TallyData>> GetMultiId(String tsNumber,String goodsID,int No);
        Task<Boolean> Update(String tsNumber,String goodsID,int No,TallyData entry);
        Task<Boolean> Delete(String tsNumber);
        Task<Boolean> Delete(String tsNumber,String goodsID,int No);
        Task<String> GetLastId();
    }
}
