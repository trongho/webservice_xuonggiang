﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataGeneralService
    {
        Task<List<WIDataGeneral>> GetAll();
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID, int Ordinal);
        Task<List<WIDataGeneral>> GetUnderId(String wIDNumber, String goodsID);
        Task<List<WIDataGeneral>> GetUnderIdCode(String wIDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(WIDataGeneral WIDataGeneral);
        Task<Boolean> Update(String wIDNumber, String goodsID, int Ordinal, WIDataGeneral WIDataGeneral);
        Task<Boolean> Delete(String wIDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wIDNumber,String goodsID);
    }
}
