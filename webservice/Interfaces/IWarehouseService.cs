﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWarehouseService
    {
        Task<List<Warehouse>> GetAll();
        Task<List<Warehouse>> GetUnderId(String id);
        Task<List<Warehouse>> GetUnderBranchID(String branchID);
        Task<Boolean> Create(Warehouse Warehouse);
        Task<Boolean> Update(String id, Warehouse Warehouse);
        Task<Boolean> Delete(String id);
        Task<Boolean> checkExist(String id);
    }
}
