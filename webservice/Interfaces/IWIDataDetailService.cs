﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice.Interfaces
{
    public interface IWIDataDetailService
    {
        Task<List<WIDataDetail>> GetAll();
        Task<List<WIDataDetail>> GetUnderId(String wIDNumber);
        Task<List<WIDataDetail>> GetUnderId(String wIDNumber, String goodsID, int Ordinal);
        Task<List<WIDataDetail>> GetUnderIdCode(String wIDNumber, String goodsID, String IDCode);
        Task<Boolean> Create(WIDataDetail WIDataDetail);
        Task<Boolean> Update(String wIDNumber, String goodsID, int Ordinal, WIDataDetail WIDataDetail);
        Task<Boolean> Delete(String wIDNumber);
        Task<String> GetLastId();
        Task<Boolean> checkExist(String wIDNumber);
    }
}
