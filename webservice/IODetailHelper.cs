﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class IODetailHelper
    {
        public static List<IODetailModel> Covert(List<IODetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new IODetailModel
            {
                IONumber = sc.IONumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsName = sc.GoodsName,
                IssueUnitID = sc.IssueUnitID,
                UnitRate = sc.UnitRate,
                StockUnitID = sc.StockUnitID,
                Quantity = sc.Quantity,
                PriceIncludedVAT = sc.PriceIncludedVAT,
                PriceExcludedVAT = sc.PriceExcludedVAT,
                Price = sc.Price,
                Discount = sc.Discount,
                DiscountAmount = sc.DiscountAmount,
                VAT = sc.VAT,
                VATAmount = sc.VATAmount,
                AmountExcludedVAT = sc.AmountExcludedVAT,
                AmountIncludedVAT = sc.AmountIncludedVAT,
                Amount = sc.Amount,
                ExpiryDate = sc.ExpiryDate,
                SerialNumber = sc.SerialNumber,
                Note = sc.Note,
                QuantityIssued = sc.QuantityIssued,
                PriceExcludedVATIssued = sc.PriceExcludedVATIssued,
                PriceIncludedVATIssued = sc.PriceIncludedVATIssued,
                PriceIssued = sc.PriceIssued,
                DiscountIssued = sc.DiscountIssued,
                DiscountAmountIssued = sc.DiscountAmountIssued,
                VATIssued = sc.VATIssued,
                VATAmountIssued = sc.VATAmountIssued,
                AmountExcludedVATIssued = sc.AmountExcludedVATIssued,
                AmountIncludedVATIssued = sc.AmountIncludedVATIssued,
                AmountIssued = sc.AmountIssued,
                QuantityOrdered = sc.QuantityOrdered,
                OrderQuantityIssued = sc.OrderQuantityIssued,
                IssueQuantityReceived = sc.IssueQuantityReceived,
                IssueQuantity = sc.IssueQuantity,
                IssueAmount = sc.IssueAmount,
                AverageCost = sc.AverageCost,
                SPSNumber = sc.SPSNumber,
                PSNumberSalePrice = sc.PSNumberSalePrice,
                PSNumberDiscount = sc.PSNumberDiscount,
                Status = sc.Status,
                SupplierCode = sc.SupplierCode,
                ASNNumber = sc.ASNNumber,
                PackingSlip = sc.PackingSlip,
            });

            return models;
        }
    }
}
