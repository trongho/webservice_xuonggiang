﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class ColorHelper
    {
        public static List<ColorModel> Covert(List<Color> entrys)
        {
            var models = entrys.ConvertAll(sc => new ColorModel
            {
              ColorID=sc.ColorID,
              ColorName=sc.ColorName,
              Description=sc.Description,
              Status=sc.Status,
              CreatedUserID=sc.CreatedUserID,
              CreatedDate=sc.CreatedDate,
              UpdatedUserID=sc.UpdatedUserID,
              UpdatedDate=sc.UpdatedDate,

            });

            return models;
        }
    }
}
