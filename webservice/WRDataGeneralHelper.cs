﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRDataGeneralHelper
    {
        public static List<WRDataGeneralModel> Covert(List<WRDataGeneral> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDataGeneralModel
            {
                WRDNumber = sc.WRDNumber,
                Ordinal = sc.Ordinal,
                Barcode = sc.Barcode,
                RFIDTagID = sc.RFIDTagID,
                StyleColorSize = sc.StyleColorSize,
                Style = sc.Style,
                Color = sc.Color,
                Sku = sc.Sku,
                Size = sc.Size,
                QuantityOrg=sc.QuantityOrg,
                Quantity = sc.Quantity,
                Gender = sc.Gender,
                ProductGroup = sc.ProductGroup,
                DescriptionVN = sc.DescriptionVN,
                LifeStylePerformance = sc.LifeStylePerformance,
                Div = sc.Div,
                Outsole = sc.Outsole,
                Category = sc.Category,
                Productline = sc.Productline,
                Description = sc.Description,
                EXWorkUSD = sc.EXWorkUSD,
                EXWorkVND = sc.EXWorkVND,
                AmountVND = sc.AmountVND,
                RetailPrice = sc.RetailPrice,
                Season = sc.Season,
                Coo = sc.Coo,
                Material = sc.Material,
                InvoiceNo = sc.InvoiceNo,
                DateOfInvoice = sc.DateOfInvoice,
                SaleContract = sc.SaleContract,
                Shipment = sc.Shipment,
                FactoryAdress = sc.FactoryAdress,
                FactoryName = sc.FactoryName,
                FactoryNameVN = sc.FactoryNameVN,
                FactoryAdressVN = sc.FactoryAdressVN,
                NewPrice = sc.NewPrice,
                ChangePrice = sc.ChangePrice,
                InputDate =sc.InputDate,
                Status=sc.Status,
                IDCode=sc.IDCode
            });

            return models;
        }
    }
}
