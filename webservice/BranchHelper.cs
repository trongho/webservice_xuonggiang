﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class BranchHelper
    {
        public static List<BranchModel> Covert(List<Branch> entrys)
        {
            var entryModels = entrys.ConvertAll(entry => new BranchModel
            {
                BranchID = entry.BranchID,
                BranchName = entry.BranchName,
                TradeName = entry.TradeName,
                Address = entry.Address,
                ProvinceID = entry.ProvinceID,
                DistrictID = entry.DistrictID,
                Representative = entry.Representative,
                Position = entry.Position,
                TaxCode = entry.TaxCode,
                TelNumber = entry.TelNumber,
                FaxNumber = entry.FaxNumber,
                Email = entry.Email,
                Website = entry.Website,
                HotlineNumber = entry.HotlineNumber,
                Fanpage = entry.Fanpage,
                BranchTypeID = entry.BranchTypeID,
                Description = entry.Description,
                Status = entry.Status,
                CreatedUserID = entry.CreatedUserID,
                CreatedDate = entry.CreatedDate,
                UpdatedUserID = entry.UpdatedUserID,
                UpdatedDate = entry.UpdatedDate,
            });

            return entryModels;
        }      
    }
}
