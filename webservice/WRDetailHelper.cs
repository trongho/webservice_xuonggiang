﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class WRDetailHelper
    {
        public static List<WRDetailModel> Covert(List<WRDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new WRDetailModel
            {
                WRNumber = sc.WRNumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsName = sc.GoodsName,
                ReceiptUnitID = sc.ReceiptUnitID,
                UnitRate = sc.UnitRate,
                StockUnitID = sc.StockUnitID,
                Quantity = sc.Quantity,
                PriceIncludedVAT = sc.PriceIncludedVAT,
                PriceExcludedVAT = sc.PriceExcludedVAT,
                Price = sc.Price,
                Discount = sc.Discount,
                DiscountAmount = sc.DiscountAmount,
                VAT = sc.VAT,
                VATAmount = sc.VATAmount,
                AmountExcludedVAT = sc.AmountExcludedVAT,
                AmountIncludedVAT = sc.AmountIncludedVAT,
                Amount = sc.Amount,
                ExpiryDate = sc.ExpiryDate,
                SerialNumber = sc.SerialNumber,
                Note = sc.Note,
                QuantityReceived = sc.QuantityReceived,
                PriceIncludedVATReceived = sc.PriceIncludedVATReceived,
                PriceExcludedVATReceived = sc.PriceExcludedVATReceived,
                PriceReceived = sc.PriceReceived,
                DiscountReceived = sc.DiscountReceived,
                DiscountAmountReceived = sc.DiscountAmountReceived,
                VATReceived = sc.VATReceived,
                VATAmountReceived = sc.VATAmountReceived,
                AmountExcludedVATReceived = sc.AmountExcludedVATReceived,
                AmountIncludedVATReceived = sc.AmountIncludedVATReceived,
                AmountReceived = sc.AmountReceived,
                QuantityOrdered = sc.QuantityOrdered,
                OrderQuantityReceived = sc.OrderQuantityReceived,
                ReceiptQuantityIssued = sc.ReceiptQuantityIssued,
                ReceiptQuantity = sc.ReceiptQuantity,
                ReceiptAmount = sc.ReceiptAmount,
                AverageCost = sc.AverageCost,
                Status = sc.Status,
                SupplierCode = sc.SupplierCode,
                ASNNumber = sc.ASNNumber,
                PackingSlip = sc.PackingSlip,

            });

            return models;
        }
    }
}
