﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class UserRightHelper
    {
        public static List<UserRightModel> CovertUserRights(List<UserRight> userRights)
        {
            var userRightModels = userRights.ConvertAll(userRight => new UserRightModel
            {
                RightID = userRight.RightID,
                Ordinal = userRight.Ordinal,
                RightName = userRight.RightName,
                RightNameEN=userRight.RightNameEN,
                UserID=userRight.UserID,
                GrantRight=userRight.GrantRight
            });

            return userRightModels;
        }
    }
}
