﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;

namespace webservice
{
    public class GoodsLineHelper
    {
        public static List<GoodsLineModel> CovertGoodsLines(List<GoodsLine> goodsLines)
        {
            var goodsLineModels = goodsLines.ConvertAll(gl => new GoodsLineModel
            {
                GoodsLineID=gl.GoodsLineID,
                GoodsLineName=gl.GoodsLineName,
                Description=gl.Description,
                Status=gl.Status,
                CreatedUserID = gl.CreatedUserID,
                CreatedDate = gl.CreatedDate,
                UpdatedUserID = gl.UpdatedUserID,
                UpdatedDate = gl.UpdatedDate,
            });
            return goodsLineModels;
        }
    }
}
