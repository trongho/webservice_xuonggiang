﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WarehouseController : ControllerBase
    {
        private readonly IWarehouseService service;

        public WarehouseController(IWarehouseService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WarehouseHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{warehouseId}")]
        public async Task<IActionResult> GetUnderId(String warehouseId)
        {
            var entrys = await service.GetUnderId(warehouseId);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WarehouseHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByBranchID/{branchId}")]
        public async Task<IActionResult> GetUnderBranchId(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WarehouseHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] Warehouse warehouse)
        {
            var entry = await service.Create(warehouse);

            return CreatedAtAction(
                 nameof(Get), new { id = warehouse.WarehouseID }, entry);
        }

        [HttpPut]
        [Route("Put/{warehouseID}")]
        public async Task<IActionResult> Update(String warehouseID, [FromBody] Warehouse warehouse)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (warehouseID != warehouse.WarehouseID)
            {
                return BadRequest();
            }

            await service.Update(warehouseID, warehouse);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{warehouseID}")]
        public async Task<IActionResult> Delete(String warehouseID)
        {
            var entry = await service.Delete(warehouseID);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
