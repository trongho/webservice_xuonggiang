﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WRDataGeneralController : ControllerBase
    {
        private readonly IWRDataGeneralService service;

        public WRDataGeneralController(IWRDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WRDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WRDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wRDNumber)
        {
            var entrys = await service.GetUnderId(wRDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WRDNumber}/{Barcode}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wRDNumber, String Barcode, int ordinal)
        {
            var entrys = await service.GetUnderId(wRDNumber, Barcode, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }


        [HttpGet]
        [Route("GetByIDCode/{WRDNumber}/{Barcode}/{IDCode}")]
        public async Task<IActionResult> GetByIDCode(String wRDNumber, String Barcode, int IDCode)
        {
            var entrys = await service.GetUnderId(wRDNumber, Barcode,IDCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        //[HttpGet]
        //[Route("GetByIDCode/{WRDNumber}/{RFIDTagID}/{*idCode}")]
        //public async Task<IActionResult> GetUnderMultiIdCode(String wRDNumber, String RFIDTagID, String IdCode)
        //{
        //    var entrys = await service.GetUnderIdCode(wRDNumber, RFIDTagID, IdCode);
        //    if (entrys == null)
        //    {
        //        return NotFound();
        //    }

        //    var entryModels = WRDataGeneralHelper.Covert(entrys);

        //    return Ok(entryModels);
        //}

        [HttpGet]
        [Route("GetByWRDNumberAndBarcode/{WRDNumber}/{Barcode}")]
        public async Task<IActionResult> GetUnderMultiID(String wRDNumber, String Barcode)
        {
            var entrys = await service.GetUnderId(wRDNumber, Barcode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WRDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WRDataGeneral WRDataGeneral)
        {
            var entry = await service.Create(WRDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WRDataGeneral.WRDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WRDNumber}/{Barcode}/{ordinal}")]
        public async Task<IActionResult> Update(String wRDNumber, String Barcode, int ordinal, [FromBody] WRDataGeneral WRDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wRDNumber != WRDataGeneral.WRDNumber)
            {
                return BadRequest();
            }

            await service.Update(wRDNumber, Barcode, ordinal, WRDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wRDNumber}")]
        public async Task<IActionResult> Delete(String wRDNumber)
        {
            var entry = await service.Delete(wRDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wRDNumber}/{Barcode}")]
        public async Task<IActionResult> checkExist(String wRDNumber,String Barcode)
        {
            var result = await service.checkExist(wRDNumber, Barcode);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWRDNumber")]
        public async Task<IActionResult> GetLastWRDNumber()
        {
            var WRDNumber = await service.GetLastId();
            if (WRDNumber == null)
            {
                return NotFound();
            }

            return Ok(WRDNumber);
        }
    }
}
