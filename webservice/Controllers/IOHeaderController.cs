﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;
using webservice.Services;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IOHeaderController : ControllerBase
    {
        private readonly IIOHeaderService service;

        public IOHeaderController(IIOHeaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = IOHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{IONumber}")]
        public async Task<IActionResult> GetIOHeaderUnderWRRNumber(String IONumber)
        {
            var entrys = await service.GetUnderId(IONumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMonthYear/{*iODate}")]
        public async Task<IActionResult> GetWRHeaderUnderWRDate(String iODate)
        {
            var entrys = await service.GetUnderMonthYear(iODate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByLastMonthYear/{*iODate}")]
        public async Task<IActionResult> GetIOHeaderUnderLastWRDate(String ioDate)
        {
            var entrys = await service.GetUnderLastMonthYear(ioDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] IOHeader IOHeader)
        {
            var entry = await service.Create(IOHeader);

            return CreatedAtAction(
                 nameof(Get), new { IONumber = IOHeader.IONumber }, entry);
        }

        [HttpPut]
        [Route("Put/{IONumber}")]
        public async Task<IActionResult> Update(String IONumber, [FromBody] IOHeader IOHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (IONumber != IOHeader.IONumber)
            {
                return BadRequest();
            }

            await service.Update(IONumber, IOHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{IONumber}")]
        public async Task<IActionResult> Delete(String IONumber)
        {
            var entry = await service.Delete(IONumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{IONumber}/")]
        public async Task<IActionResult> checkExist(String IONumber)
        {
            var result = await service.checkExist(IONumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastIONumber")]
        public async Task<IActionResult> GetLastIONumber()
        {
            var IONumber = await service.GetLastId();
            if (IONumber == null)
            {
                return NotFound();
            }

            return Ok(IONumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetWRRHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await service.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetWRRHeaderUnderBranchID(String branchID)
        {
            var entrys = await service.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await service.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByModality/{modalityID}")]
        public async Task<IActionResult> GetWRHeaderUnderModalityID(String modalityID)
        {
            var entrys = await service.GetUnderModalityID(modalityID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = IOHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}

