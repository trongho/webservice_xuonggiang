﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TSDataGeneralController : ControllerBase
    {
        private readonly ITSDataGeneralService service;

        public TSDataGeneralController(ITSDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("GetByID/{warehouseID}/{*TallyDate}")]
        public async Task<IActionResult> GetUnderID(String warehouseID,DateTime TallyDate)
        {
            var entrys = await service.GetUnderId(warehouseID,TallyDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{warehouseID}/{GoodsID}/{Ordinal}/{*TallyDate}")]
        public async Task<IActionResult> GetUnderMultiId(String warehouseID, DateTime TallyDate,String goodsID, int ordinal)
        {
            var entrys = await service.GetMultiId(warehouseID,TallyDate, ordinal,goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = TSDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] TSDataGeneral entry)
        {
            var entrys = await service.Create(entry);

            return CreatedAtAction(
                 nameof(GetUnderID), new { WarehouseID = entry.WarehouseID,TallyDate=entry.TallyDate,
                    GoodsID=entry.GoodsID,Ordinal=entry.Ordinal}, entry);
        }

        [HttpPut]
        [Route("Put/{warehouseID}/{GoodsID}/{Ordinal}/{*TallyDate}")]
        public async Task<IActionResult> Update(String warehouseID, DateTime TallyDate, String goodsID, int ordinal, [FromBody] TSDataGeneral entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (warehouseID != entry.WarehouseID&&TallyDate!=entry.TallyDate&&goodsID!=entry.GoodsID&&ordinal!=entry.Ordinal)
            {
                return BadRequest();
            }

            await service.Update(warehouseID, TallyDate, ordinal,goodsID, entry);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{warehouseID}/{*TallyDate}")]
        public async Task<IActionResult> Delete(String warehouseID,DateTime TallyDate)
        {
            var entry = await service.Delete(warehouseID, TallyDate);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }
    }
}
