﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RFIDReaderController : ControllerBase
    {
        private readonly IRFIDReaderService service;

        public RFIDReaderController(IRFIDReaderService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = RFIDReaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{id}")]
        public async Task<IActionResult> GetUnderID(String id)
        {
            var entrys = await service.GetUnderId(id);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = RFIDReaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] RFIDReader entry)
        {
            var entrys = await service.Create(entry);

            return CreatedAtAction(
                 nameof(Get), new { HostName = entry.HostName }, entry);
        }

        [HttpPut]
        [Route("Put/{id}")]
        public async Task<IActionResult> Update(String id, [FromBody] RFIDReader entry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != entry.HostName)
            {
                return BadRequest();
            }

            await service.Update(id, entry);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{id}")]
        public async Task<IActionResult> Delete(String id)
        {
            var entry = await service.Delete(id);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{id}/")]
        public async Task<IActionResult> checkExist(String id)
        {
            var result = await service.checkExist(id);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }
    }
}
