﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataGeneralController : ControllerBase
    {
        private readonly IWIDataGeneralService service;

        public WIDataGeneralController(IWIDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WIDNumber}")]
        public async Task<IActionResult> GetWRDataGeneralUnderWRDNumber(String wIDNumber)
        {
            var entrys = await service.GetUnderId(wIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WIDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String wIDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{WIDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String wIDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(wIDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByWIDNumberAndGoodsID/{WIDNumber}/{goodsID}")]
        public async Task<IActionResult> GetUnderMultiGoodsID(String wIDNumber, String goodsID)
        {
            var entrys = await service.GetUnderId(wIDNumber, goodsID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataGeneral WIDataGeneral)
        {
            var entry = await service.Create(WIDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { WRDNumber = WIDataGeneral.WIDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WIDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String wIDNumber, String goodsID, int ordinal, [FromBody] WIDataGeneral WIDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (wIDNumber != WIDataGeneral.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(wIDNumber, goodsID, ordinal, WIDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{wIDNumber}")]
        public async Task<IActionResult> Delete(String wIDNumber)
        {
            var entry = await service.Delete(wIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{wIDNumber}/{goodsID}")]
        public async Task<IActionResult> checkExist(String wIDNumber, String goodsID)
        {
            var result = await service.checkExist(wIDNumber, goodsID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIDNumber")]
        public async Task<IActionResult> GetLastWIDNumber()
        {
            var WIDNumber = await service.GetLastId();
            if (WIDNumber == null)
            {
                return NotFound();
            }

            return Ok(WIDNumber);
        }
    }
}
