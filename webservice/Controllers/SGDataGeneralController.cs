﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SGDataGeneralController : ControllerBase
    {
        private readonly ISGDataGeneralService service;

        public SGDataGeneralController(ISGDataGeneralService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = SGDataGeneralHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{SGDNumber}")]
        public async Task<IActionResult> GetSGDataGeneralUnderSGDNumber(String SGDNumber)
        {
            var entrys = await service.GetUnderId(SGDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{SGDNumber}/{RFIDTagID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String SGDNumber, String RFIDTagID, int ordinal)
        {
            var entrys = await service.GetUnderId(SGDNumber, RFIDTagID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{SGDNumber}/{RFIDTagID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String SGDNumber, String RFIDTagID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(SGDNumber, RFIDTagID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetBySGDNumberAndRFIDTagID/{SGDNumber}/{RFIDTagID}")]
        public async Task<IActionResult> GetUnderMultiID(String SGDNumber, String RFIDTagID)
        {
            var entrys = await service.GetUnderId(SGDNumber, RFIDTagID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataGeneralHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] SGDataGeneral SGDataGeneral)
        {
            var entry = await service.Create(SGDataGeneral);

            return CreatedAtAction(
                 nameof(Get), new { SGDNumber = SGDataGeneral.SGDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{SGDNumber}/{RFIDTagID}/{ordinal}")]
        public async Task<IActionResult> Update(String SGDNumber, String RFIDTagID, int ordinal, [FromBody] SGDataGeneral SGDataGeneral)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (SGDNumber != SGDataGeneral.SGDNumber)
            {
                return BadRequest();
            }

            await service.Update(SGDNumber, RFIDTagID, ordinal, SGDataGeneral);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{SGDNumber}")]
        public async Task<IActionResult> Delete(String SGDNumber)
        {
            var entry = await service.Delete(SGDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{SGDNumber}/{RFIDTagID}")]
        public async Task<IActionResult> checkExist(String SGDNumber, String RFIDTagID)
        {
            var result = await service.checkExist(SGDNumber, RFIDTagID);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastSGDNumber")]
        public async Task<IActionResult> GetLastSGDNumber()
        {
            var SGDNumber = await service.GetLastId();
            if (SGDNumber == null)
            {
                return NotFound();
            }

            return Ok(SGDNumber);
        }
    }
}
