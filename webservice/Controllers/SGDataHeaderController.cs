﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SGDataHeaderController : ControllerBase
    {
        private readonly ISGDataHeaderService SGDataHeaderService;

        public SGDataHeaderController(ISGDataHeaderService SGDataHeaderService)
        {
            this.SGDataHeaderService = SGDataHeaderService;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await SGDataHeaderService.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = SGDataHeaderHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{sGDNumber}")]
        public async Task<IActionResult> GetSGDataHeaderUndersGDNumber(String sGDNumber)
        {
            var entrys = await SGDataHeaderService.GetUnderId(sGDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] SGDataHeader SGDataHeader)
        {
            var entry = await SGDataHeaderService.Create(SGDataHeader);

            return CreatedAtAction(
                 nameof(Get), new { WRRNumber = SGDataHeader.SGDNumber }, entry);
        }

        [HttpPut]
        [Route("Put/{sGDNumber}")]
        public async Task<IActionResult> Update(String sGDNumber, [FromBody] SGDataHeader SGDataHeader)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (sGDNumber != SGDataHeader.SGDNumber)
            {
                return BadRequest();
            }

            await SGDataHeaderService.Update(sGDNumber, SGDataHeader);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{sGDNumber}")]
        public async Task<IActionResult> Delete(String sGDNumber)
        {
            var entry = await SGDataHeaderService.Delete(sGDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{sGDNumber}/")]
        public async Task<IActionResult> checkExist(String sGDNumber)
        {
            var result = await SGDataHeaderService.checkExist(sGDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastsGDNumber")]
        public async Task<IActionResult> GetLastsGDNumber()
        {
            var sGDNumber = await SGDataHeaderService.GetLastId();
            if (sGDNumber == null)
            {
                return NotFound();
            }

            return Ok(sGDNumber);
        }

        [HttpGet]
        [Route("FilterByDate/{fromDate}/{toDate}")]
        public async Task<IActionResult> GetSGDataHeaderUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = await SGDataHeaderService.GetUnderDate(fromDate, toDate);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByBranch/{branchID}")]
        public async Task<IActionResult> GetSGDataHeaderUnderBranchID(String branchID)
        {
            var entrys = await SGDataHeaderService.GetUnderBranchID(branchID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByHandlingStatus/{handlingStatusID}")]
        public async Task<IActionResult> GetWRRHeaderUnderHandlingStatusID(String handlingStatusID)
        {
            var entrys = await SGDataHeaderService.GetUnderHandlingStatusID(handlingStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("FilterByOrderStatus/{orderStatusID}")]
        public async Task<IActionResult> GetSGDataHeaderUnderOrderStatusID(String orderStatusID)
        {
            var entrys = await SGDataHeaderService.GetUnderOrderStatusID(orderStatusID);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = SGDataHeaderHelper.Covert(entrys);

            return Ok(entryModels);
        }
    }
}
