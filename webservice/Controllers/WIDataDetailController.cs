﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WIDataDetailController : ControllerBase
    {
        private readonly IWIDataDetailService service;

        public WIDataDetailController(IWIDataDetailService service)
        {
            this.service = service;
        }

        [HttpGet]
        [Route("")]
        public async Task<IActionResult> Get()
        {
            var entrys = await service.GetAll();
            if (entrys == null)
            {
                return NotFound();
            }
            var entryModels = WIDataDetailHelper.Covert(entrys);
            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByID/{WIDNumber}")]
        public async Task<IActionResult> GetWRDataDetailUnderWIDNumber(String WIDNumber)
        {
            var entrys = await service.GetUnderId(WIDNumber);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByMultiID/{WIDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> GetUnderMultiId(String WIDNumber, String goodsID, int ordinal)
        {
            var entrys = await service.GetUnderId(WIDNumber, goodsID, ordinal);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpGet]
        [Route("GetByIDCode/{WIDNumber}/{goodsID}/{*idCode}")]
        public async Task<IActionResult> GetUnderMultiIdCode(String WIDNumber, String goodsID, String IdCode)
        {
            var entrys = await service.GetUnderIdCode(WIDNumber, goodsID, IdCode);
            if (entrys == null)
            {
                return NotFound();
            }

            var entryModels = WIDataDetailHelper.Covert(entrys);

            return Ok(entryModels);
        }

        [HttpPost]
        [Route("Post")]
        public async Task<IActionResult> Create([FromBody] WIDataDetail wIDataDetail)
        {
            var entry = await service.Create(wIDataDetail);

            return CreatedAtAction(
                 nameof(Get), new { WIDNumber = wIDataDetail.WIDNumber }, entry);
        }


        [HttpPut]
        [Route("Put/{WIDNumber}/{goodsID}/{ordinal}")]
        public async Task<IActionResult> Update(String WIDNumber, String goodsID, int ordinal, [FromBody] WIDataDetail wIDataDetail)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (WIDNumber != wIDataDetail.WIDNumber)
            {
                return BadRequest();
            }

            await service.Update(WIDNumber, goodsID, ordinal, wIDataDetail);

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("Delete/{WIDNumber}")]
        public async Task<IActionResult> Delete(String WIDNumber)
        {
            var entry = await service.Delete(WIDNumber);
            if (entry == null)
            {
                return NotFound();
            }

            return Ok(entry);
        }

        [HttpGet]
        [Route("CheckExist/{WIDNumber}/")]
        public async Task<IActionResult> checkExist(String WIDNumber)
        {
            var result = await service.checkExist(WIDNumber);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        [HttpGet]
        [Route("lastWIDNumber")]
        public async Task<IActionResult> GetLastWIDNumber()
        {
            var WIDNumber = await service.GetLastId();
            if (WIDNumber == null)
            {
                return NotFound();
            }

            return Ok(WIDNumber);
        }
    }
}
