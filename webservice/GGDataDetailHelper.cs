﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class GGDataDetailHelper
    {
        public static List<GGDataDetailModel> Covert(List<GGDataDetail> entrys)
        {
            var models = entrys.ConvertAll(sc => new GGDataDetailModel
            {
                GGDNumber = sc.GGDNumber,
                Ordinal = sc.Ordinal,
                GoodsID = sc.GoodsID,
                GoodsName = sc.GoodsName,
                IDCode = sc.IDCode,
                LocationID = sc.LocationID,
                Quantity = sc.Quantity,
                TotalQuantity = sc.TotalQuantity,
                TotalGoods = sc.TotalGoods,
                QuantityOrg = sc.QuantityOrg,
                TotalQuantityOrg = sc.TotalGoodsOrg,
                TotalGoodsOrg = sc.TotalGoodsOrg,
                LocationIDOrg = sc.LocationIDOrg,
                CreatorID = sc.CreatorID,
                CreatedDateTime = sc.CreatedDateTime,
                EditerID = sc.EditerID,
                EditedDateTime = sc.EditedDateTime,
                Status = sc.Status,
                PackingVolume = sc.PackingVolume,
                QuantityByPack = sc.QuantityByPack,
                QuantityByItem = sc.QuantityByItem,
                Note = sc.Note,
            });

            return models;
        }
    }
}
