﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRRDetailService:IWRRDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRRDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wRRNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRRDetails.FirstOrDefault(g => g.WRRNumber.Equals(wRRNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.AddAsync(wRRDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(o => o.WRRNumber.Equals(id));
            warehouseDbContext.WRRDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRRDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WRRDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRRDetails.OrderByDescending(x => x.WRRNumber).Take(1).Select(x => x.WRRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string Barcode, int Ordinal)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber)&&u.Barcode.Equals(Barcode) &&u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRRDetail>> GetUnderId(string wRRNumber, string Barcode)
        {
            var entrys = warehouseDbContext.WRRDetails.Where(u => u.WRRNumber.Equals(wRRNumber) && u.Barcode.Equals(Barcode));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wRRNumber, String barcode, int Ordinal, WRRDetail wRRDetail)
        {
            var entrys = warehouseDbContext.WRRDetails.FirstOrDefault(o => o.WRRNumber.Equals(wRRNumber) && o.Barcode.Equals(barcode) && o.Ordinal==Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRRDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
