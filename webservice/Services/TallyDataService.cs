﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class TallyDataService: ITallyDataService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public TallyDataService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }


        public async Task<bool> Create(TallyData entry)
        {
            var entrys = warehouseDbContext.TallyDatas.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string tsNumber)
        {
            var entrys = warehouseDbContext.TallyDatas.Where(o => o.TSNumber.Equals(tsNumber));
            warehouseDbContext.TallyDatas.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(string tsNumber, string goodsID, int No)
        {
            var entrys = warehouseDbContext.TallyDatas.Where(o => o.TSNumber.Equals(tsNumber)
                && o.GoodsID.Equals(goodsID)&& o.No.Equals(No));
            warehouseDbContext.TallyDatas.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.TallyDatas.OrderByDescending(x => x.TSNumber).Take(1).Select(x => x.TSNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<TallyData>> GetMultiId(string tsNumber, string goodsID, int No)
        {
            var entrys = warehouseDbContext.TallyDatas.Where(u => u.TSNumber.Equals(tsNumber) && u.GoodsID.Equals(goodsID) && u.No == No);
            return await entrys.ToListAsync();
        }

        public async Task<List<TallyData>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.TallyDatas.Where(u => u.TSNumber.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string tsNumber, string goodsID, int No, TallyData entry)
        {
            var entrys = warehouseDbContext.TallyDatas.FirstOrDefault(o => o.TSNumber.Equals(tsNumber) && o.GoodsID.Equals(goodsID) && o.No == No);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
