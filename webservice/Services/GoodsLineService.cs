﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GoodsLineService : IGoodsLineService
    {
        private readonly WarehouseDbContext warehouseDbContext;
        private readonly IHostingEnvironment _hostingEnvironment;

        public GoodsLineService(WarehouseDbContext warehouseDbContext, IHostingEnvironment hostingEnvironment)
        {
            this.warehouseDbContext = warehouseDbContext;
            _hostingEnvironment = hostingEnvironment;
        }

        public List<GoodsLine> ImportGoodsLine()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"importGoodsLine.xlsx";
            FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));
            

            using (ExcelPackage package = new ExcelPackage(file))
            {

                ExcelWorksheet workSheet = package.Workbook.Worksheets["GoodsLine"];
                int totalRows = workSheet.Dimension.Rows;

                List<GoodsLine> goodsLineList = new List<GoodsLine>();

                for (int i = 2; i <= totalRows; i++)
                {
                    goodsLineList.Add(new GoodsLine
                    {
                        GoodsLineID = workSheet.Cells[i, 1].Value.ToString(),
                        GoodsLineName = workSheet.Cells[i, 2].Value.ToString(),
                        Description = workSheet.Cells[i, 3].Value.ToString()
                    });

                    //remove exist record
                    foreach (GoodsLine goodsLine in goodsLineList.ToList())
                    {
                        if (warehouseDbContext.GoodsLines.Any(u => u.GoodsLineID.Equals(goodsLine.GoodsLineID)))
                        {
                            goodsLineList.Remove(goodsLine);
                        }
                    }

                }

                warehouseDbContext.GoodsLines.AddRange(goodsLineList);
                warehouseDbContext.SaveChanges();

                return goodsLineList;
            }
        }

        public string ExportGoodsLine()
        {
            string rootFolder = _hostingEnvironment.WebRootPath;
            string fileName = @"exportGoodsLine.xlsx";
            string result = "";

            FileInfo file = new FileInfo(Path.Combine(rootFolder, fileName));

            using (ExcelPackage package = new ExcelPackage(file))
            {

                List<GoodsLine> goodsLineList = warehouseDbContext.GoodsLines.ToList();

                if (package.Workbook.Worksheets.Count>1)
                {
                    result = "Non empty worksheet";
                }
                else
                {
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("GoodsLine");
                    int totalRows = goodsLineList.Count();

                    worksheet.Cells[1, 1].Value = "GoodsLineID";
                    worksheet.Cells[1, 2].Value = "GoodsLineName";
                    worksheet.Cells[1, 3].Value = "Description";
                    worksheet.Cells[1, 4].Value = "Status";
                    int i = 0;
                    for (int row = 2; row <= totalRows + 1; row++)
                    {
                        worksheet.Cells[row, 1].Value = goodsLineList[i].GoodsLineID;
                        worksheet.Cells[row, 2].Value = goodsLineList[i].GoodsLineName;
                        worksheet.Cells[row, 3].Value = goodsLineList[i].Description;
                        worksheet.Cells[row, 4].Value = goodsLineList[i].Status;
                        i++;
                    }

                    result = "GoodsLine list has been exported successfully";
                }              
                package.Save();
                
            }

            return result;
        }

        public async Task<List<GoodsLine>> GetAllGoodsLine()
        {
            var goodsLines = warehouseDbContext.GoodsLines;
            return await goodsLines.ToListAsync();
        }

        public async Task<bool> CreateGoodsLine(GoodsLine goodsLine)
        {
            var goodsLines = warehouseDbContext.GoodsLines.AddAsync(goodsLine);
            warehouseDbContext.SaveChanges();

            return goodsLines.IsCompleted;
        }

        public async Task<GoodsLine> GetGoodsLineUnderId(string id)
        {
            var goodsLine = warehouseDbContext.GoodsLines.Where(u => u.GoodsLineID.Equals(id)).FirstOrDefault();
            return goodsLine;
        }

        public async Task<bool> UpdateGoodsLine(string id, GoodsLine goodsLine)
        {
            var goodsLines = warehouseDbContext.GoodsLines.FirstOrDefault(o => o.GoodsLineID.Equals(id));
            
            warehouseDbContext.Entry(goodsLines).CurrentValues.SetValues(goodsLine);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public bool DeleteGoodsLine(string id)
        {
            try
            {
                var goodLines = warehouseDbContext.GoodsLines.Where(o => o.GoodsLineID.Equals(id)).FirstOrDefault();
                warehouseDbContext.GoodsLines.Remove(goodLines);
                warehouseDbContext.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public async Task<Boolean> checkExist(string id)
        {
            Boolean flag=false;
            var goodsLine = warehouseDbContext.GoodsLines.FirstOrDefault(g=>g.GoodsLineID.Equals(id));
            if (goodsLine != null)
            {
                flag=true;
            }
            return flag;
        }
    }
}
