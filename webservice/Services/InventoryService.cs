﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryService : IInventoryService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year==year
            &&g.Month==month&&g.WarehouseID.Equals(warehouseID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID, string goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Inventory inventory)
        {
            var entrys = warehouseDbContext.Inventorys.AddAsync(inventory);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string goodsID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year==year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            warehouseDbContext.Inventorys.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.Inventorys.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Inventory>> GetAll()
        {
            var entrys = warehouseDbContext.Inventorys;
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<Inventory>> GetUnderId(short year, short month, string warehouseID, string goodsID)
        {
            var entrys = warehouseDbContext.Inventorys.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string goodsID, Inventory inventory)
        {
            var entrys = warehouseDbContext.Inventorys.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(inventory);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
