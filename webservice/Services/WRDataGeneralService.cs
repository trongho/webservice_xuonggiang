﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRDataGeneralService: IWRDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wRDNumber,String Barcode)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(g => g.WRDNumber.Equals(wRDNumber)&&g.Barcode.Equals(Barcode));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRDataGeneral WRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.AddAsync(WRDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(o => o.WRDNumber.Equals(wRDNumber));
            warehouseDbContext.WRDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.WRDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRDataGenerals.OrderByDescending(x => x.WRDNumber).Take(1).Select(x => x.WRDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string Barcode, int Ordinal)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.Barcode.Equals(Barcode) && u.Ordinal==Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> GetUnderId(string wRDNumber, string Barcode)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.Barcode.Equals(Barcode));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRDataGeneral>> CheckIdCode(string wRDNumber, string Barcode, string IDCode)
        {
            var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.Barcode.Equals(Barcode) && u.IDCode.Contains(IDCode));
            return await entrys.ToListAsync();
        }

        //public async Task<List<WRDataGeneral>> GetUnderIdCode(string wRDNumber, string RFIDTagID, string IDCode)
        //{
        //    int idcodeLenght = IDCode.Length;
        //    int percentIndex = IDCode.IndexOf("%");
        //    String s1 = IDCode.Substring(0, 1);
        //    String s2 = IDCode.Substring(percentIndex, 3);
        //    String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

        //    s2 = s2.Replace(s2, "/");
        //    IDCode = s1 + s2 + s3;

        //    var entrys = warehouseDbContext.WRDataGenerals.Where(u => u.WRDNumber.Equals(wRDNumber) && u.GoodsID.Equals(RFIDTagID) && u.IDCode == IDCode);
        //    return await entrys.ToListAsync();
        //}

        public async Task<bool> Update(string wRDNumber, string Barcode, int Ordinal, WRDataGeneral WRDataGeneral)
        {
            var entrys = warehouseDbContext.WRDataGenerals.FirstOrDefault(o => o.WRDNumber.Equals(wRDNumber) && o.Barcode.Equals(Barcode) && o.Ordinal==Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WRDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
