﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class RFIDSignalService : IRFIDSignalService
    {
        private readonly WarehouseDbContext warehouseDbContext;
        public RFIDSignalService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.RFIDSignals.FirstOrDefault(g => g.RFIDTag.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(RFIDSignal entry)
        {
            var entrys = warehouseDbContext.RFIDSignals.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.RFIDSignals.FirstOrDefault(o => o.RFIDTag.Equals(id));
            warehouseDbContext.RFIDSignals.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<RFIDSignal>> GetAll()
        {
            var entrys = warehouseDbContext.RFIDSignals;
            return await entrys.ToListAsync();
        }

        public async Task<List<RFIDSignal>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.RFIDSignals.Where(u => u.RFIDTag.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, RFIDSignal entry)
        {
            var entrys = warehouseDbContext.RFIDSignals.FirstOrDefault(o => o.RFIDTag.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
