﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class SGDataGeneralService: ISGDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public SGDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string SGDNumber, String goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.SGDataGenerals.FirstOrDefault(g => g.SGDNumber.Equals(SGDNumber) && g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(SGDataGeneral SGDataGeneral)
        {
            var entrys = warehouseDbContext.SGDataGenerals.AddAsync(SGDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string SGDNumber)
        {
            var entrys = warehouseDbContext.SGDataGenerals.Where(o => o.SGDNumber.Equals(SGDNumber));
            warehouseDbContext.SGDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<SGDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.SGDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.SGDataGenerals.OrderByDescending(x => x.SGDNumber).Take(1).Select(x => x.SGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<SGDataGeneral>> GetUnderId(string SGDNumber)
        {
            var entrys = warehouseDbContext.SGDataGenerals.Where(u => u.SGDNumber.Equals(SGDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataGeneral>> GetUnderId(string SGDNumber, string RFIDTagID, int Ordinal)
        {
            var entrys = warehouseDbContext.SGDataGenerals.Where(u => u.SGDNumber.Equals(SGDNumber) && u.RFIDTagID.Equals(RFIDTagID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataGeneral>> GetUnderId(string SGDNumber, string RFIDTagID)
        {
            var entrys = warehouseDbContext.SGDataGenerals.Where(u => u.SGDNumber.Equals(SGDNumber) && u.RFIDTagID.Equals(RFIDTagID));
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataGeneral>> GetUnderIdCode(string SGDNumber, string RFIDTagID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            s2 = s2.Replace(s2, "/");
            IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.SGDataGenerals.Where(u => u.SGDNumber.Equals(SGDNumber) && u.GoodsID.Equals(RFIDTagID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string SGDNumber, string RFIDTagID, int Ordinal, SGDataGeneral SGDataGeneral)
        {
            var entrys = warehouseDbContext.SGDataGenerals.FirstOrDefault(o => o.SGDNumber.Equals(SGDNumber) && o.RFIDTagID.Equals(RFIDTagID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(SGDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
