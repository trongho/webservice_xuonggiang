﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class RFIDReaderService : IRFIDReaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;
        public RFIDReaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.RFIDReaders.FirstOrDefault(g => g.HostName.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(RFIDReader entry)
        {
            var entrys = warehouseDbContext.RFIDReaders.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.RFIDReaders.FirstOrDefault(o => o.HostName.Equals(id));
            warehouseDbContext.RFIDReaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<RFIDReader>> GetAll()
        {
            var entrys = warehouseDbContext.RFIDReaders;
            return await entrys.ToListAsync();
        }

        public async Task<List<RFIDReader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.RFIDReaders.Where(u => u.HostName.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, RFIDReader entry)
        {
            var entrys = warehouseDbContext.RFIDReaders.FirstOrDefault(o => o.HostName.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
