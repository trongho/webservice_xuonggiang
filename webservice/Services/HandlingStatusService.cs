﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class HandlingStatusService : IHandlingStatusService
    {

        private readonly WarehouseDbContext warehouseDbContext;
        public HandlingStatusService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }
        public async Task<List<HandlingStatus>> GetAll()
        {
            var entrys = warehouseDbContext.HandlingStatuss;
            return await entrys.ToListAsync();
        }
    }
}
