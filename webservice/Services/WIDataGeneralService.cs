﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIDataGeneralService: IWIDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wIDNumber,String goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WIDataGenerals.FirstOrDefault(g => g.WIDNumber.Equals(wIDNumber)&&g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WIDataGeneral WIDataGeneral)
        {
            var entrys = warehouseDbContext.WIDataGenerals.AddAsync(WIDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(o => o.WIDNumber.Equals(wIDNumber));
            warehouseDbContext.WIDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WIDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.WIDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WIDataGenerals.OrderByDescending(x => x.WIDNumber).Take(1).Select(x => x.WIDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WIDataGeneral>> GetUnderId(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataGeneral>> GetUnderId(string wIDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataGeneral>> GetUnderId(string wIDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber) && u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataGeneral>> GetUnderIdCode(string wIDNumber, string goodsID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            s2 = s2.Replace(s2, "/");
            IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.WIDataGenerals.Where(u => u.WIDNumber.Equals(wIDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wIDNumber, string goodsID, int Ordinal, WIDataGeneral WIDataGeneral)
        {
            var entrys = warehouseDbContext.WIDataGenerals.FirstOrDefault(o => o.WIDNumber.Equals(wIDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WIDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
