﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class InventoryOfLastMonthsService: IInventoryOfLastMonthsService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public InventoryOfLastMonthsService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> checkExist(short year, short month, string warehouseID, string goodsID)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(InventoryOfLastMonths InventoryOfLastMonths)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.AddAsync(InventoryOfLastMonths);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID, string goodsID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<bool> Delete(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            warehouseDbContext.InventoryOfLastMonthss.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<InventoryOfLastMonths>> GetAll()
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss;
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderId(short year, short month, string warehouseID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID));
            return await entrys.ToListAsync();
        }

        public async Task<List<InventoryOfLastMonths>> GetUnderId(short year, short month, string warehouseID, string goodsID)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.Where(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(short year, short month, string warehouseID, string goodsID, InventoryOfLastMonths InventoryOfLastMonths)
        {
            var entrys = warehouseDbContext.InventoryOfLastMonthss.FirstOrDefault(g => g.Year == year
            && g.Month == month && g.WarehouseID.Equals(warehouseID) && g.GoodsID.Equals(goodsID));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(InventoryOfLastMonths);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
