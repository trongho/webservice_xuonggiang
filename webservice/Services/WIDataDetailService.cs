﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WIDataDetailService : IWIDataDetailService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WIDataDetailService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string wIDNumber)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WIDataDetails.FirstOrDefault(g => g.WIDNumber.Equals(wIDNumber));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WIDataDetail WIDataDetail)
        {
            var entrys = warehouseDbContext.WIDataDetails.AddAsync(WIDataDetail);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(o => o.WIDNumber.Equals(wIDNumber));
            warehouseDbContext.WIDataDetails.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WIDataDetail>> GetAll()
        {
            var entrys = warehouseDbContext.WIDataDetails;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WIDataDetails.OrderByDescending(x => x.WIDNumber).Take(1).Select(x => x.WIDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WIDataDetail>> GetUnderId(string wIDNumber)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataDetail>> GetUnderId(string wIDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<WIDataDetail>> GetUnderIdCode(string wIDNumber, string goodsID, string IDCode)
        {
            int idcodeLenght = IDCode.Length;
            int percentIndex = IDCode.IndexOf("%");
            String s1 = IDCode.Substring(0, 1);
            String s2 = IDCode.Substring(percentIndex, 3);
            String s3 = IDCode.Substring(percentIndex + 3, idcodeLenght - s1.Length - s2.Length);

            s2 = s2.Replace(s2, "/");
            IDCode = s1 + s2 + s3;

            var entrys = warehouseDbContext.WIDataDetails.Where(u => u.WIDNumber.Equals(wIDNumber) && u.GoodsID.Equals(goodsID) && u.IDCode == IDCode);
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string wIDNumber, string goodsID, int Ordinal, WIDataDetail WIDataDetail)
        {
            var entrys = warehouseDbContext.WIDataDetails.FirstOrDefault(o => o.WIDNumber.Equals(wIDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(WIDataDetail);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
