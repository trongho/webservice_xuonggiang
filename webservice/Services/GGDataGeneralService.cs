﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class GGDataGeneralService : IGGDataGeneralService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public GGDataGeneralService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string gGDNumber, string goodIDs)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.GGDataGenerals.FirstOrDefault(g => g.GGDNumber.Equals(gGDNumber) && g.GoodsID.Equals(goodIDs));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(GGDataGeneral GGDataGeneral)
        {
            var entrys = warehouseDbContext.GGDataGenerals.AddAsync(GGDataGeneral);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string gGDNumber)
        {
            var entrys = warehouseDbContext.GGDataGenerals.Where(o => o.GGDNumber.Equals(gGDNumber));
            warehouseDbContext.GGDataGenerals.RemoveRange(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<GGDataGeneral>> GetAll()
        {
            var entrys = warehouseDbContext.GGDataGenerals;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.GGDataGenerals.OrderByDescending(x => x.GGDNumber).Take(1).Select(x => x.GGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<GGDataGeneral>> GetUnderId(string gGDNumber)
        {
            var entrys = warehouseDbContext.GGDataGenerals.Where(u => u.GGDNumber.Equals(gGDNumber));
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataGeneral>> GetUnderId(string gGDNumber, string goodsID, int Ordinal)
        {
            var entrys = warehouseDbContext.GGDataGenerals.Where(u => u.GGDNumber.Equals(gGDNumber) && u.GoodsID.Equals(goodsID) && u.Ordinal == Ordinal);
            return await entrys.ToListAsync();
        }

        public async Task<List<GGDataGeneral>> GetUnderId(string gGDNumber, string goodsID)
        {
            var entrys = warehouseDbContext.GGDataGenerals.Where(u => u.GGDNumber.Equals(gGDNumber) && u.GoodsID.Equals(goodsID));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string gGDNumber, string goodsID, int Ordinal, GGDataGeneral GGDataGeneral)
        {
            var entrys = warehouseDbContext.GGDataGenerals.FirstOrDefault(o => o.GGDNumber.Equals(gGDNumber) && o.GoodsID.Equals(goodsID) && o.Ordinal == Ordinal);
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(GGDataGeneral);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
