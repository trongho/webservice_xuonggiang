﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class UnitService:IUnitService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public UnitService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.Units.FirstOrDefault(g => g.UnitID.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Unit entry)
        {
            var entrys = warehouseDbContext.Units.AddAsync(entry);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.Units.FirstOrDefault(o => o.UnitID.Equals(id));
            warehouseDbContext.Units.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Unit>> GetAll()
        {
            var entrys = warehouseDbContext.Units;
            return await entrys.ToListAsync();
        }

        public async Task<List<Unit>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.Units.Where(u => u.UnitID.Equals(id));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, Unit entry)
        {
            var entrys = warehouseDbContext.Units.FirstOrDefault(o => o.UnitID.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
