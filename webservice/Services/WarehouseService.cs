﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WarehouseService : IWarehouseService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WarehouseService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entry = warehouseDbContext.Warehouses.FirstOrDefault(g => g.WarehouseID.Equals(id));
            if (entry != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(Warehouse Warehouse)
        {
            var entry = warehouseDbContext.Warehouses.AddAsync(Warehouse);
            warehouseDbContext.SaveChanges();
            return entry.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entry = warehouseDbContext.Warehouses.FirstOrDefault(o => o.WarehouseID.Equals(id));
            warehouseDbContext.Warehouses.Remove(entry);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<Warehouse>> GetAll()
        {
            var entrys = warehouseDbContext.Warehouses;
            return await entrys.ToListAsync();
        }

        public async Task<List<Warehouse>> GetUnderBranchID(string branchID)
        {
            var entry = warehouseDbContext.Warehouses.Where(u => u.BranchID.Equals(branchID));
            return await entry.ToListAsync();
        }

        public async Task<List<Warehouse>> GetUnderId(string id)
        {
            var entry = warehouseDbContext.Warehouses.Where(u => u.WarehouseID.Equals(id));
            return await entry.ToListAsync();
        }

        public async Task<bool> Update(string id, Warehouse Warehouse)
        {
            var entry = warehouseDbContext.Warehouses.FirstOrDefault(o => o.WarehouseID.Equals(id));
            warehouseDbContext.Entry(entry).CurrentValues.SetValues(Warehouse);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
