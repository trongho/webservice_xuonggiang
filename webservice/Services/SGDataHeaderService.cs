﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class SGDataHeaderService: ISGDataHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public SGDataHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.SGDataHeaders.FirstOrDefault(g => g.SGDNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(SGDataHeader SGDataHeader)
        {
            var entrys = warehouseDbContext.SGDataHeaders.AddAsync(SGDataHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.SGDataHeaders.FirstOrDefault(o => o.SGDNumber.Equals(id));
            warehouseDbContext.SGDataHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }


        public async Task<List<SGDataHeader>> GetAll()
        {
            var entrys = warehouseDbContext.SGDataHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.SGDataHeaders.OrderByDescending(x => x.SGDNumber).Take(1).Select(x => x.SGDNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<SGDataHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.SGDataHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.SGDataHeaders.Where(u => u.SGDDate >= fromDate && u.SGDDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.SGDataHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataHeader>> GetUnderOrderStatusID(string orderStatusID)
        {
            var entrys = warehouseDbContext.SGDataHeaders.Where(u => u.Status.Equals(orderStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<SGDataHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.SGDataHeaders.Where(u => u.SGDNumber.Equals(id));
            return await entrys.ToListAsync();
        }


        public async Task<bool> Update(string id, SGDataHeader SGDataHeader)
        {
            var entrys = warehouseDbContext.SGDataHeaders.FirstOrDefault(o => o.SGDNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(SGDataHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }
    }
}
