﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class ScreenService : IScreenService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public ScreenService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<List<Screen>> GetAllScreen()
        {
            var screens = warehouseDbContext.Screens;
            return await screens.ToListAsync();
        }
    }
}
