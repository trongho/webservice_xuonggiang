﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class ModalityService : IModalityService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public ModalityService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<List<Modality>> GetAll()
        {
            var entrys = warehouseDbContext.Modalitys;
            return await entrys.ToListAsync();
        }
    }
}
