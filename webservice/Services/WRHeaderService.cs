﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Interfaces;

namespace webservice.Services
{
    public class WRHeaderService:IWRHeaderService
    {
        private readonly WarehouseDbContext warehouseDbContext;

        public WRHeaderService(WarehouseDbContext warehouseDbContext)
        {
            this.warehouseDbContext = warehouseDbContext;
        }

        public async Task<bool> checkExist(string id)
        {
            Boolean flag = false;
            var entrys = warehouseDbContext.WRHeaders.FirstOrDefault(g => g.WRNumber.Equals(id));
            if (entrys != null)
            {
                flag = true;
            }
            return flag;
        }

        public async Task<bool> Create(WRHeader wRHeader)
        {
            var entrys = warehouseDbContext.WRHeaders.AddAsync(wRHeader);
            warehouseDbContext.SaveChanges();

            return entrys.IsCompleted;
        }

        public async Task<bool> Delete(string id)
        {
            var entrys = warehouseDbContext.WRHeaders.FirstOrDefault(o => o.WRNumber.Equals(id));
            warehouseDbContext.WRHeaders.Remove(entrys);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRHeader>> GetAll()
        {
            var entrys = warehouseDbContext.WRHeaders;
            return await entrys.ToListAsync();
        }

        public async Task<string> GetLastId()
        {
            var curentID = warehouseDbContext.WRHeaders.OrderByDescending(x => x.WRNumber).Take(1).Select(x => x.WRNumber).ToList().FirstOrDefault();
            return curentID;
        }

        public async Task<List<WRHeader>> GetUnderBranchID(string branchID)
        {
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.BranchID.Equals(branchID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRHeader>> GetUnderDate(DateTime fromDate, DateTime toDate)
        {
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.WRDate >= fromDate && u.WRDate <= toDate);
            return await entrys.ToListAsync();
        }

        public async Task<List<WRHeader>> GetUnderHandlingStatusID(string handlingStatusID)
        {
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.HandlingStatusID.Equals(handlingStatusID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRHeader>> GetUnderModalityID(string modalityID)
        {
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.ModalityID.Equals(modalityID));
            return await entrys.ToListAsync();
        }

        public async Task<List<WRHeader>> GetUnderId(string id)
        {
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.WRNumber.Equals(id));
            return await entrys.ToListAsync();
        }


        public async Task<List<WRHeader>> GetUnderWRDate(String monthYear)
        {
            String year =monthYear.Substring(3,4);
            String month = monthYear.Substring(0,2);
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.WRDate.Value.Year == int.Parse(year) && u.WRDate.Value.Month == int.Parse(month));
            return await entrys.ToListAsync();
        }

        public async Task<bool> Update(string id, WRHeader wRHeader)
        {
            var entrys = warehouseDbContext.WRHeaders.FirstOrDefault(o => o.WRNumber.Equals(id));
            warehouseDbContext.Entry(entrys).CurrentValues.SetValues(wRHeader);
            warehouseDbContext.SaveChanges();
            return true;
        }

        public async Task<List<WRHeader>> GetUnderLastWRDate(string monthYear)
        {
            String year = monthYear.Substring(3, 4);
            String month = monthYear.Substring(0, 2);
            int yearInt = int.Parse(year);
            int monthInt = int.Parse(month);
            int mLastMonth;
            int mLastYear;
            if (monthInt == 1)
            {
                mLastMonth = 12;
                mLastYear = yearInt - 1;
            }
            else
            {
                mLastMonth = monthInt - 1;
                mLastYear = yearInt;
            }
            if (monthInt == 1)
            {
                mLastMonth = 12;
                mLastYear = yearInt - 1;
            }
            else
            {
                mLastMonth = monthInt - 1;
                mLastYear = yearInt;
            }
            var entrys = warehouseDbContext.WRHeaders.Where(u => u.WRDate.Value.Year != mLastYear|| u.WRDate.Value.Month<=monthInt);
            return await entrys.ToListAsync();
        }
    }
}
