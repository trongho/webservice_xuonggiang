﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class SGDataHeaderHelper
    {
        public static List<SGDataHeaderModel> Covert(List<SGDataHeader> entrys)
        {
            var models = entrys.ConvertAll(sc => new SGDataHeaderModel
            {
                SGDNumber = sc.SGDNumber,
                SGDDate = sc.SGDDate,
                ReferenceNumber = sc.ReferenceNumber,
                WRRNumber = sc.WRRNumber,
                WRRReference = sc.WRRReference,
                HandlingStatusID = sc.HandlingStatusID,
                HandlingStatusName = sc.HandlingStatusName,
                Note = sc.Note,
                BranchID = sc.BranchID,
                BranchName = sc.BranchName,
                TotalQuantity = sc.TotalQuantity,
                TotalQuantityOrg = sc.TotalQuantityOrg,
                Status = sc.Status,
                CreatedUserID = sc.CreatedUserID,
                CreatedDate = sc.CreatedDate,
                UpdatedUserID = sc.UpdatedUserID,
                UpdatedDate = sc.UpdatedDate,
            });

            return models;
        }
    }
}
