﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using webservice.Entities;
using webservice.Models;

namespace webservice
{
    public class InventoryOfLastMonthsHelper
    {
        public static List<InventoryOfLastMonthsModel> Covert(List<InventoryOfLastMonths> entrys)
        {
            var models = entrys.ConvertAll(sc => new InventoryOfLastMonthsModel
            {
                Year = sc.Year,
                Month = sc.Month,
                WarehouseID = sc.WarehouseID,
                GoodsID = sc.GoodsID,
                OpeningStockQuantity = sc.OpeningStockQuantity,
                OpeningStockAmount = sc.OpeningStockAmount,
                ReceiptQuantity = sc.ReceiptQuantity,
                ReceiptAmount = sc.ReceiptAmount,
                IssueQuantity = sc.IssueQuantity,
                IssueAmount = sc.IssueAmount,
                ClosingStockQuantity = sc.ClosingStockQuantity,
                ClosingStockAmount = sc.ClosingStockAmount,
                AverageCost = sc.AverageCost,
                Status = sc.Status,
            });

            return models;
        }
    }
}
